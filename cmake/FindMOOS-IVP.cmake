# - Find libpcap
# Find the native PCAP includes and library
#
#  MOOS_IVP_INCLUDE_DIRS - where to find pcap.h, etc.
#  MOOS_IVP_LIBRARIES    - List of libraries when using pcap.
#  MOOS_IVP_FOUND        - True if pcap found.

# First, try pkg-config
find_package(PkgConfig)
pkg_search_module(PC_MOOS_IVP MOOS)


if(NOT PC_MOOS_IVP_FOUND)

    # this will be the path for docker builds:
    SET(MOOS_IVP_PATH /workspaces/moos-ivp)

    find_path(MOOS_IVP_BINARY_PATH
        NAMES
            MOOSDB
        PATHS
            "${MOOS_IVP_PATH}/bin"
        REQUIRED
    )

    # # unaccounted for: 
    # ${MOOS_IVP_PATH}/build/MOOS/MOOSGeodesy/lib/libMOOSGeodesy.so
    # ${MOOS_IVP_PATH}/build/MOOS/proj-5.2.0/lib64/libproj.a
    # ${MOOS_IVP_PATH}/build/MOOS/proj-5.2.0/lib/libproj.a

    find_path(MOOS_LINK_PATH
        NAMES
            libMOOS.a
        PATHS
            "${MOOS_IVP_PATH}/build/MOOS/MOOSCore/lib"
        REQUIRED
    )
    find_path(IVP_LINK_PATH
        NAMES
            libivpcore.a
        PATHS
            "${MOOS_IVP_PATH}/lib"
        REQUIRED
    )
    # set(_pkg_required_vars MOOS_IVP_LINK_PATH MOOS_IVP_INCLUDE_PATH)
endif()

# FILE(GLOB IVP_INCLUDE_DIRS ${MOOSIVP_SOURCE_TREE_BASE}/ivp/src/lib_* )
# INCLUDE_DIRECTORIES(${IVP_INCLUDE_DIRS})

# FILE(GLOB IVP_LIBRARY_DIRS ${MOOSIVP_SOURCE_TREE_BASE}/lib )
# LINK_DIRECTORIES(${IVP_LIBRARY_DIRS})

if(MOOS_IVP_BINARY_PATH)

    set(MOOS_FOUND 1)
    set(MOOS_IVP_FOUND 1)

    set( MOOS_IVP_BINARY_PATHS ${MOOS_IVP_BINARY_PATH} )

    LIST(APPEND
        MOOS_IVP_INCLUDE_PATHS
            "${MOOS_IVP_PATH}/include/ivp"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/App/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Comms/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/DB/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Utils/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Thirdparty/PocoBits/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Thirdparty/getpot/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Thirdparty/AppCasting/include"
    )
    set( MOOS_IVP_LINK_PATHS ${MOOS_LINK_PATH} ${IVP_LINK_PATH})

    FILE(GLOB MOOS_LIBRARIES RELATIVE ${MOOS_LINK_PATH} ${MOOS_LINK_PATH}/lib* )
    FILE(GLOB IVP_LIBRARIES RELATIVE ${IVP_LINK_PATH} ${IVP_LINK_PATH}/lib* )
    set( MOOS_IVP_LIBRARIES ${MOOS_LIBRARIES} ${IVP_LIBRARIES} )

    # not sure what the value is for this....
    mark_as_advanced( MOOS_BINARIES )
    mark_as_advanced( MOOS_INCLUDES )
    mark_as_advanced( MOOS_LIBRARIES )
    mark_as_advanced( MOOS_IVP_BINARY_PATHS )
    mark_as_advanced( MOOS_IVP_INCLUDE_PATHS )
    mark_as_advanced( MOOS_IVP_LINK_PATHS )
    mark_as_advanced( MOOS_IVP_LIBRARIES )
endif()
