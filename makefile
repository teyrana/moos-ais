BUILD_TYPE="debug"
SHELL:=bash

# local paths (host system)
CONTEXT_PATH=${PWD}

# container parameters (container / client system)
DOCKER_CONTAINER_NAME=moos-ais-vscode-dev
DOCKER_FILE_PATH=${CONTEXT_PATH}/.devcontainer/Dockerfile
DOCKER_IMAGE_TAG=moos-ais
DOCKER_IMAGE_LABEL=devel
DOCKER_PROJECT_PATH=/workspaces/moos-ais
DOCKER_USER="vscode"

#-------------------------------------------------------------------
#  Part 2: Invoke the call to make in the build directory
#-------------------------------------------------------------------

# default target goes first
.PHONY: default
default: mvp

# -------------------- Define Rest of Targets -------------------------------

attach: docker-attach

.PHONY: build
build/src/ingest: build
build/src/trackmon: build
build: #build/CMakeCache.txt
	cd build && ninja

.PHONY: config, configure
config:configure
build/CMakeCache.txt: configure
configure:
	cd build && cmake -GNinja -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..

docker-attach:
	docker exec -it -w ${DOCKER_PROJECT_PATH} ${DOCKER_CONTAINER_NAME} ${SHELL}

docker-build: 
	docker build --tag=${DOCKER_IMAGE_TAG}:${DOCKER_IMAGE_LABEL} --file=${DOCKER_FILE_PATH} ${CONTEXT_PATH}

docker-exec: docker-attach

run: docker-run
docker-run:DOCKER_MOUNT_ARGS="type=bind,source=${CONTEXT_PATH},destination=${DOCKER_PROJECT_PATH}"
docker-run: docker-build
	docker run -it --rm -w ${DOCKER_PROJECT_PATH} --mount=${DOCKER_MOUNT_ARGS} ${DOCKER_IMAGE_TAG}:${DOCKER_IMAGE_LABEL}


mvp: build
	build/src/counter


clean: 
	rm -rf build/*

